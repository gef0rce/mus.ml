import os
import getopt, sys
import pickle
import features
import subprocess
import pandas as pd
import numpy as np
import operator



def build_db_from_csv(input_folder):
    df = pd.DataFrame()
    for dirname, subdirs, filenames in os.walk(input_folder):
        for filename in filenames:
            if filename.endswith(".csv"):
                print "processing: " + filename
                df = df.append(pd.read_csv(os.path.join(dirname, filename)))
    return df

def find_similar(f, base, d_count):
	ans = []
	for index,row in db.iterrows():
		tmp = np.array(row[list("ABCDEFGHIJKLM")])
		dist = np.linalg.norm(f-tmp)
		ans.append((index, dist, row['_artist'], row['_title']))
	ans = sorted(ans, key=lambda x: x[1])
	return ans[:d_count]



def usage():
	print """Usage: prototype.py --input=foo.mp3 --temp=./data/wav/tmp.wav
        
        This script finds similar tracks in local database for 'foo.mp3'
        This requires sox, numpy, pandas, twisted
        """


if __name__ == "__main__":

	# default params
	input_file = None  	# csv
	temp_file = "./tmp.wav"
	display_count = 5
	db_path = "./data/db.pickled"

	# parse arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hi:t:d:", ["help", "input=","temp=", "display="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-i", "--input"):
			input_file = a
		elif o in ("-t", "--temp"):
			temp_file = a
		elif o in ("-d", "--display"):
			display_count = a	
		else:
			assert False, "unhandled option"
	if input_file is None:
		print("Too few params.")
		usage()
		sys.exit(2)

	# init db
	print 'Init db...'
	db = pickle.load(open(db_path, 'r'))

	# convert file
	print 'Convert to wav...'
	subprocess.call(["mpg123", "-1", "-w", temp_file, input_file])

	# extract features
	print 'Extract features...'
	[data, dataVar] = features.extract(temp_file)

	# getting list of similar tracks
	print 'Searching for similar tracks...'
	sim_tracks = find_similar(data, db, display_count)

	# result
	print 'Similar tracks:'
	for ind, track in enumerate(sim_tracks):
		out_string = str(ind+1) + ". " + track[2] +  " - " + track[3]
		if len(out_string) < 60:
			space_filled = 60 - len(out_string) 
		else:
			space_filled = 5
		out_string = out_string + " " * space_filled + "[dist=" + str(track[1]) + "]"
		print out_string

