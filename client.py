from twisted.internet import reactor
from twisted.internet.protocol import ClientCreator
from twisted.protocols import amp
import os
import getopt, sys
import pickle
import features
import subprocess
import pandas as pd
import numpy as np
import operator



class Classify(amp.Command):
    # normally shared by client and server code
    arguments = [('data',     amp.ListOf(amp.Float())),
                 ('dataVar',  amp.ListOf(amp.Float()))]
    response = [('result',    amp.ListOf(amp.String()))]

def connected(protocol, data, dataVar):
    return protocol.callRemote(Classify, data=data, dataVar=dataVar).addCallback(gotResult)

def gotResult(result):
    # result
    print 'Similar tracks:'
    for str in result["result"]:
        print str
    reactor.stop()    

def error(reason):
    print "Something went wrong"
    print reason
    reactor.stop()    


def usage():
	print """Usage: client.py --input=foo.mp3 [--addr=127.0.0.1 --temp=temp.wav --port=1234]
        
This script finds similar tracks in server database for 'foo.mp3'
Required packages: mpg123, python2-numpy, python2-pandas, python2-twisted
"""


if __name__ == "__main__":

	# default params
	input_file = None  	# csv
	temp_file = "./tmp.wav"
	display_count = 5
	db_path = "./data/db.pickled"
        address = "127.0.0.1"
        port = 1234

	# parse arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hi:t:d:p:a:", ["help", "input=","temp=", "display=", "port=", "addr="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-i", "--input"):
			input_file = a
		elif o in ("-t", "--temp"):
			temp_file = a
		elif o in ("-d", "--display"):
			display_count = a	
                elif o in ("-a", "--addr"):
                        address = a
                elif o in ("-p", "--port"):
                        port = int(a)
		else:
			assert False, "unhandled option"
	if input_file is None:
		print("Too few params.")
		usage()
		sys.exit(2)

	# convert file
	print 'Convert to wav...'
	subprocess.call(["mpg123", "-1", "-w", temp_file, input_file])

	# extract features
	print 'Extract features...'
	[data, dataVar] = features.extract(temp_file)

        ClientCreator(reactor, amp.AMP).connectTCP(address, port).addCallback(connected, data, dataVar).addErrback(error)

        reactor.run()
