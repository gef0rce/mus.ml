import os
import getopt, sys
import pickle
import pandas as pd




def build_db_from_csv(input_folder):
    df = pd.DataFrame()
    for dirname, subdirs, filenames in os.walk(input_folder):
        for filename in filenames:
            if filename.endswith(".csv"):
                print "processing: " + filename
                df = df.append(pd.read_csv(os.path.join(dirname, filename)))
    return df


def usage():
	print "Need to write something there :) "


if __name__ == "__main__":
	input_dir = None  	# csv
	output_file = None	# pickled

	# parse arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hi:o:", ["help", "input=","output="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-i", "--input"):
			input_dir = a
		elif o in ("-o", "--output"):
			output_file = a
		else:
			assert False, "unhandled option"
	if input_dir is None or output_file is None:
		print("Too few params.")
		usage()
		sys.exit(2)

	db = build_db_from_csv(input_dir)
	pickle.dump(db, open(output_file, "w"), 2)