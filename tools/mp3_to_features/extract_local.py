import os
import getopt, sys
import subprocess
import features
import numpy as np
import pandas as pd
from mutagen.easyid3 import EasyID3
import threading

# parse options
def usage():
	print("See script source for details")
	#print("Hint: python ./tools/mp3_to_features/extract.py --input=./data/mp3/ --temp=./data/wav/ --output=./data/uid.csv")
	print("Hint: python ./tools/mp3_to_features/extract_local.py --input=./data/mp3/ --temp=./data/wav/tmp.wav --output=./data/uid.csv")


def process_folder(input_folder, temp_file, output_file):
	# walk in dir
	df = pd.DataFrame()
	for dirname, subdirs, filenames in os.walk(input_folder):
	    for filename in filenames:
	        if filename.endswith(".mp3"):
	        	# convert to wav
	        	#wav_filename = temp_file
	        	fullpath_mp3 = os.path.join(os.path.abspath(input_folder), filename)
	        	fullpath_wav = temp_file
	        	try:
	        		tags = EasyID3(fullpath_mp3)
	        		meta = pd.DataFrame({"_artist":tags['artist'], "_title": tags['title'], "_genre":[-1]})
	        	except:
	        		print "[s] " + os.path.join(dirname, filename)
	        		continue
	        	#os.path.join(os.path.abspath(temp_folder), wav_filename)
	        	subprocess.call(["sox", fullpath_mp3, "-c 1", fullpath_wav]) #monochannel?
	        	print "[+] " + fullpath_mp3
	        	# extract features
	        	[data, dataVar] = features.extract(fullpath_wav)
	        	tokens = filename[:-len(".mp3")].split("_._")
	        	
	        	featuresMean = pd.DataFrame([data], columns=list("ABCDEFGHIJKLM"))
	        	featuresVar = pd.DataFrame([dataVar], columns=list("abcdefghijklm"))
	        	song_row = pd.concat([meta, featuresMean, featuresVar], axis=1)
	        	# save result
	        	df = df.append(song_row)
	        	#clean wav
	        	#subprocess.call(["rm", fullpath_wav])
	        else:
	        	print "[s] " + os.path.join(dirname, filename)
	#save df
	try:
		if not df.empty:
			df.to_csv(output_file, index=False)
	except:
		print "Failed at saving dataframe:" + output_file
	

def thread_worker(partial_dirlist, temp_file, output_dir, thread_num):
	for folder in partial_dirlist:
		out_filename = "th"+str(thread_num)+ "." + os.path.basename(folder)[-20:] + ".csv"
		print(out_filename)
		process_folder(folder, temp_file, os.path.join(output_dir, out_filename))



if __name__ == "__main__":

	input_folder = None  	#mp3
	temp_folder = None		#wav
	output_dir = None	#features

	# parse arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hi:t:o:", ["help", "input=","temp=","output="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-i", "--input"):
			input_folder = a
		elif o in ("-t", "--temp"):
			temp_folder = a	
		elif o in ("-o", "--output"):
			output_dir = a
		else:
			assert False, "unhandled option"
	if input_folder is None or output_dir is None or temp_folder is None:
		print("Too few params.")
		usage()
		sys.exit(2)

	#get dir list
	dirlist = []
	for dirname, dirnames, filenames in os.walk(input_folder):
		for subdirname in dirnames:
			dirlist.append(os.path.join(dirname, subdirname))

	# 4 hardcoded threads
	th1 = threading.Thread(target=thread_worker, args=[dirlist[0::4], temp_folder+"/1.wav", output_dir, 1])
	th2 = threading.Thread(target=thread_worker, args=[dirlist[1::4], temp_folder+"/2.wav", output_dir, 2])
	th3 = threading.Thread(target=thread_worker, args=[dirlist[2::4], temp_folder+"/3.wav", output_dir, 3])
	th4 = threading.Thread(target=thread_worker, args=[dirlist[3::4], temp_folder+"/4.wav", output_dir, 4])

	th1.start()
	th2.start()
	th3.start()
	th4.start()


	th4.join()
	th3.join()
	th2.join()
	th1.join()
