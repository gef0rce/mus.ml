import os
import getopt, sys
import subprocess
import features
import numpy as np
import pandas as pd

# parse options
def usage():
	print("See script source for details")
	print("Hint: python ./tools/mp3_to_features/extract.py --input=./data/mp3/ --temp=./data/wav/ --output=./data/uid.csv")

def process_folder(input_folder, temp_folder, output_file):
	# walk in dir
	df = pd.DataFrame()
	for dirname, subdirs, filenames in os.walk(input_folder):
	    for filename in filenames:
	        if filename.endswith(".mp3"):
	        	# convert to wav
	        	wav_filename = filename[:-len(".mp3")] + ".wav" # cut .mp3
	        	fullpath_mp3 = os.path.join(os.path.abspath(input_folder), filename)
	        	fullpath_wav = os.path.join(os.path.abspath(temp_folder), wav_filename)
	        	subprocess.call(["sox", fullpath_mp3, "-c 1", fullpath_wav]) #monochannel?
	        	print "[+] " + fullpath_wav
	        	# extract features
	        	[data, dataVar] = features.extract(fullpath_wav)
	        	tokens = filename[:-len(".mp3")].split("_._")
	        	meta = pd.DataFrame({"_artist":[tokens[1]], "_title": [tokens[2]], "_genre":[tokens[0]]})
	        	featuresMean = pd.DataFrame([data], columns=list("ABCDEFGHIJKLM"))
	        	featuresVar = pd.DataFrame([dataVar], columns=list("abcdefghijklm"))
	        	song_row = pd.concat([meta, featuresMean, featuresVar], axis=1)
	        	# save result
	        	df = df.append(song_row)
	        	#clean wav
	        	subprocess.call(["rm", fullpath_wav])
	        else:
	        	print "[s] " + os.path.join(dirname, filename)
	#save df
	df.to_csv(output_file, index=False)





if __name__ == "__main__":

	input_folder = None  	#mp3
	temp_folder = None		#wav
	output_file = None	#features

	# parse arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hi:t:o:", ["help", "input=","temp=","output="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-i", "--input"):
			input_folder = a
		elif o in ("-t", "--temp"):
			temp_folder = a	
		elif o in ("-o", "--output"):
			output_file = a
		else:
			assert False, "unhandled option"
	if input_folder is None or output_file is None or temp_folder is None:
		print("Too few params.")
		usage()
		sys.exit(2)

	process_folder(input_folder, temp_folder, output_file)




