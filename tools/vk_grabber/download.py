## -*- coding: utf-8 -*-
import pickle
import os
import getopt, sys
import sys
import urllib as ur
import threading


def download_track(info):
	delim = "_._"
	#file_dir = "./data/mp3/"
	file_name = str(info['genre']) + delim + info['artist'] + delim + info['title']
	file_path =  output_dir + file_name + '.mp3' 
	cut_pos = info['url'].find("?")
	link = info['url'][:cut_pos]

	# check for existing such file
	if os.path.isfile(file_path):
		return "[exist] " + file_name

	# try to download
	try:
		ur.urlretrieve(link, file_path)
	except:
		file_name = "[error] " + file_name

	return file_name


def thread_worker(partial_tracklist):
	for track_info in partial_tracklist:
		file_name = download_track(track_info)
		print "[+] " + file_name


def usage():
	print("Hint: python tools/vk_grabber/download.py --input=./data/tracklists/UID.pickled --output=./data/mp3")


if __name__ == "__main__":

	input_file = None  	# Pickled dump file
	output_dir = None	# dir for store *.mp3

	# parse arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hi:o:", ["help", "input=","output="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-i", "--input"):
			input_file = a
		elif o in ("-o", "--output"):
			output_dir = a
		else:
			assert False, "unhandled option"
	if output_dir is None or input_file is None:
		print("Too few params.")
		usage()
		sys.exit(2)

	# read tracklist
	tracklist = pickle.load(open(input_file, "r"))

	# 4 hardcoded threads
	th1 = threading.Thread(target=thread_worker, args=[tracklist[0::4]])
	th2 = threading.Thread(target=thread_worker, args=[tracklist[1::4]])
	th3 = threading.Thread(target=thread_worker, args=[tracklist[2::4]])
	th4 = threading.Thread(target=thread_worker, args=[tracklist[3::4]])

	th1.start()
	th2.start()
	th3.start()
	th4.start()

	th4.join()
	th3.join()
	th2.join()
	th1.join()

	print("[!] Done!")


