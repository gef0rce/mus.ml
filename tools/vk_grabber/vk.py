import os
import json
import vk_auth, vk_api
import config
from collections import defaultdict



def load_json(filename):
	with open(filename, 'r') as f:
		return json.load(f)

def save_json(filename, obj):
	with open(filename, 'w') as f:
		json.dump(obj, f, sort_keys=True, indent=4)


class VKApi(object):
	def __init__(self, email, password, client_id, scope):
		self.email = email
		self.password = password
		self.client_id = client_id
		self.scope = scope

		self.token = None
		self.user_id = None

	def ensure_login(self):
		if self.token is None or self.user_id is None:
			self.token, self.user_id = vk_auth.auth(
				self.email, self.password, client_id=self.client_id, scope=self.scope)

	def get_tracks(self, uid):
		# load previous list or download new
		if config.debug_mode and os.path.exists(config.audios_cache):
			audios = load_json(config.audios_cache)
		else:
			self.ensure_login()
			params = {'owner_id': uid}
			audios = vk_api.call_api('audio.get', params, self.token)[1:]  # The first item is the length
			save_json(config.audios_cache, audios)
			
			if config.debug_mode:
				save_json(config.audios_cache, audios)

		tracks = []

		#import ipdb; ipdb.set_trace()

		for audio in audios:
			track_info = {}
			track_info['artist'] = audio['artist']
			track_info['title'] = audio['title']

			# not all track have "genre" field
			try:
				track_info['genre'] = audio['genre']
			except:
				track_info['genre'] = -1

			track_info['url'] = audio['url']
			
			tracks.append(track_info)

		return tracks


def grabItForTracklist(email, password, uid):
	vk = VKApi(
		email=email,
		password=password,
		client_id=config.AppID,
		scope=['audio']
		)

	tracks = vk.get_tracks(uid)
	return tracks
		
