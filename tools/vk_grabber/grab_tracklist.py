from pprint import pprint
import pickle
import sys
import vk
import config
import os
import getopt, sys

#import ipdb; ipdb.set_trace()


#uid = 6866725 #Mari Vikhreva
#uid = 4145607 #Sergey Dovgal
#uid = 605838 #Mikhail Trofimov

def usage():
	print "Hint: python tools/vk_grabber/grab_tracklist.py --uid=UID --output=./data/tracklists/UID.pickled"

if __name__ == "__main__":

	uid = None  		# VK User ID
	output_file = None	# Pickled dump file

	# parse arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hu:o:", ["help", "uid=","output="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-u", "--uid"):
			uid = a
		elif o in ("-o", "--output"):
			output_file = a
		else:
			assert False, "unhandled option"
	if output_file is None or uid is None:
		print("Too few params.")
		usage()
		sys.exit(2)



	print '[?]\t Getting list of tracks for uid=' + str(uid) + '...'

	tracklist = vk.grabItForTracklist(config.email, config.password, uid)

	print '[+]\t...done!'
	print 'Tracklist contains ' + str(len(tracklist)) + ' tracks'
	print 'Head objects:\n'

	pprint(tracklist[:2])
	
	print "\n[?] Pickling to " + output_file + "..."
	pickle.dump(tracklist, open(output_file, "w"), 2)
 



