from twisted.protocols import amp
from twisted.internet import reactor
from twisted.internet.protocol import Factory
import os 
import getopt, sys
import pickle
import features
import subprocess
import pandas as pd
import numpy as np
import operator

def build_db_from_csv(input_folder):
    df = pd.DataFrame()
    for dirname, subdirs, filenames in os.walk(input_folder):
        for filename in filenames:
            if filename.endswith(".csv"):
                print "processing: " + filename
                df = df.append(pd.read_csv(os.path.join(dirname, filename)))
    return df

def find_similar(f, base, d_count):
	ans = []
	for index,row in db.iterrows():
		tmp = np.array(row[list("ABCDEFGHIJKLM")])
		dist = np.linalg.norm(f-tmp)
		ans.append((index, dist, row['_artist'], row['_title']))
	ans = sorted(ans, key=lambda x: x[1])
	return ans[:d_count]

def usage():
	print """This is track similarity finding server.
If you want to search for some similar tracks, use client.py"""

db_path = "./data/db.pickled"
db = pickle.load(open(db_path, 'r'))
display_count = 5

class Classify(amp.Command):
    arguments = [('data', amp.ListOf(amp.Float())),
                 ('dataVar',  amp.ListOf(amp.Float()))]
    response = [('result', amp.ListOf(amp.String()))]

class Protocol(amp.AMP):
    @Classify.responder
    def classify(self, data, dataVar): 
	sim_tracks = find_similar(data, db, display_count)
        result = []
        for ind, track in enumerate(sim_tracks):
                out_string = str(ind+1) + ". " + track[2] +  " - " + track[3]
                if len(out_string) < 60:
                        space_filled = 60 - len(out_string) 
                else:
                        space_filled = 5
                out_string = out_string + " " * space_filled + "[dist=" + str(track[1]) + "]"
                result.append(out_string)
        return {'result': result}

if __name__ == "__main__":

        #default params
        port = 1234

	# parse arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hp:", ["help", "port="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
                elif o in ("-p", "--port"):
                        port = int(a)
		else:
			assert False, "unhandled option"

        usage()

        pf = Factory()
        pf.protocol = Protocol
        reactor.listenTCP(port, pf) # listen on port 1234
        reactor.run()

        sys.exit(0)

	# getting list of similar tracks
	print 'Searching for similar tracks...'
	sim_tracks = find_similar(data, db, display_count)

	# result
	print 'Similar tracks:'
	for ind, track in enumerate(sim_tracks):
		print str(ind+1) + ". " + track[2] +  " - " + track[3] + "\t\t[dist=" + str(track[1]) + "]"

